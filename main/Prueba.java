package main;

public class Prueba implements Comparable{
	private int comparador;
	
	public int getComparador() {
		return comparador;
	}
	@Override
	public int compareTo(Object o) {
		Prueba actual = (Prueba)o;
		if(comparador < actual.getComparador()) {
			return -1;
		}else if(comparador > actual.getComparador()) {
			return 1;
		}else {
			return 0;
		}	
	}
}
