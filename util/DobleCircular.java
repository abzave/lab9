package util;
import interfaces.Lista;

public class DobleCircular<T extends Comparable> implements Lista<T> {
	private Nodo<T> cabeza;
	private Nodo<T> cola;
	private int contadorElementos = 0;
	
	public void limpiar() {
		cabeza = null;
		cola = null;
		contadorElementos = 0;
	}
	
	public int tamano() {
		return contadorElementos;
	}
	
	public int buscarElemento(T elemento) {
		for(int i = 0; i < contadorElementos; i++){
            if(elemento == getNodeAtPos(i)){
                return i;
            }
        }
        return -1;
	}
	
	public boolean agregarElemento(T value) {
		Nodo<T> newNode = new Nodo<T>(value);
        if(value == null || buscarElemento(value) != -1){
            return false;
        }
        contadorElementos++;
		if (contadorElementos != 0) {
            Nodo<T> siguiente = getInsertionNode(value);
            linkNodes(siguiente.getPrevious(), newNode, siguiente);
            return true;
		} else {
			cabeza = newNode;
			cola = newNode;
			linkNodes(cabeza, cola);
            return true;
		}
	}

	public boolean eliminarElemento(T elemento) {
		if(contadorElementos != 0) {
			Nodo<T> searchPointer = cabeza;
			for(int i = 0; i<contadorElementos; i++) {
				if(searchPointer.getValue().compareTo(elemento)==0) {
					removeNode(searchPointer);
					return true; 
				}
				searchPointer = searchPointer.getNext();
			}
		}
		return false;
	}
	
	public boolean eliminarElemento(int indice) {
		if (contadorElementos != 0 && indice< contadorElementos) {
			Nodo<T> searchPointer = getNodeAtPos(indice);
			removeNode(searchPointer);
			return true;
		}
		return false;
	}

	
	private void linkNodes(Nodo<T> pPrevious, Nodo<T> pNext) {
		pPrevious.setNext(pNext);
		pNext.setPrevious(pPrevious);
	}
	
	private void linkNodes(Nodo<T> pPrevious, Nodo<T> pActual, Nodo<T> pNext) {
		linkNodes(pPrevious,pActual);
		linkNodes(pActual,pNext);
	}
	
	private Nodo<T> getNodeAtPos(int index){
		Nodo<T> searchPointer = cabeza;
		for (int i = 0; i< index; i++) {
			searchPointer = searchPointer.getNext();
		}
		return searchPointer;
	}
	
    private Nodo<T> getInsertionNode(T element){
    	Nodo<T> searchPointer = cabeza;
		for (int i = 0; i < contadorElementos; i++) {
            if(element.compareTo(searchPointer) == 0){
                return null;
            }else if(element.compareTo(searchPointer) == 1){
                return searchPointer;
            }
            searchPointer = searchPointer.getNext();
		}
		return cola;
	}

	private void removeNode(Nodo<T> searchPointer) {
		if(searchPointer == cabeza) {
			cabeza = cabeza.getNext();
			linkNodes(cola, cabeza);
		}else if(searchPointer == cola) {
			cola = cola.getPrevious();
			linkNodes(cola, cabeza);
		}else {
			linkNodes(searchPointer.getPrevious(), searchPointer.getNext());
		}
		contadorElementos--;
	}
	@Override
	public String toString() {
		String list = "[";
		Nodo<T> searchPointer = cabeza; 
		for(int i = 0; i<contadorElementos; i++) {
			list+= searchPointer.getValue()+",";
			searchPointer = searchPointer.getNext();
		}
		if(list.length() == 1) {
			return list+"]";
		}
		return list.substring(0, list.length()-1)+"]";
	}
	
	public String toStringOrdenInverso() {
		String list = "[";
		Nodo<T> searchPointer = cola; 
		for(int i = 0; i<contadorElementos; i++) {
			list+= searchPointer.getValue()+",";
			searchPointer = searchPointer.getPrevious();
		}
		if(list.length() == 1) {
			return list+"]";
		}
		return list.substring(0, list.length()-1)+"]";
	}
	
}
