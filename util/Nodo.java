package util;

public class Nodo<T> {
	private T value	;
	private Nodo<T> Next;
	private Nodo<T> Previous;
	
	public Nodo(T pValue) {
		value = pValue;
	}
    
	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		value = value;
	}

	public Nodo<T> getNext() {
		return Next;
	}

	public void setNext(Nodo<T> next) {
		Next = next;
	}
	public Nodo<T> getPrevious() {
		return Previous;
	}

	public void setPrevious(Nodo<T> previous) {
		Previous = previous;
	}

    public String toString(){
        return value.toString(); 
    }
	
}
