/**
 * Modelo de comportamiento de una lista.
 * 
 * @author Abraham Meza y Tribeth Rivas
 * @version 22/10/18
 */

package interfaces;

public interface Lista<E>{
  boolean agregarElemento(E e);
  int buscarElemento(E e);
  boolean eliminarElemento(E e);
  boolean eliminarElemento(int indice);
  void limpiar();
  int tamano();
  String toString();
  String toStringOrdenInverso();
}
